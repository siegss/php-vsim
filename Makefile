################################################################################
#
# Build script for Vendor Similarity
#
################################################################################

# Root
export SRCROOT=$(shell pwd)
# Target
EXECUTABLE	:= libvsim.so.1.0
TARGET_TYPE := elf
# Cuda source files (compiled with cudacc)
CUFILES		:= 
# C/C++ source files (compiled with gcc / c++)
CCFILES		:=
CFILES      :=  VendorSimilarity.c
# Subdirectories
DEPS        :=  Engine.a Algorithms.a
# Additional libraries needed by the project
USECUFFT    := 1

export LD_LIBRARY_PATH=/usr/local/cuda/lib64
export CFLAGS += -std=c99 -Wno-missing-prototypes -I/usr/include/mysql -DBIG_JOINS=1  -fno-strict-aliasing -fopenmp -DUNIV_LINUX
export CXXFLAGS += -fopenmp
LDFLAGS +=  -lgomp -lmysqlclient -lpthread -shared -Wl,-soname,libvsim.so

include Makefile.common

################################################################################
#
# Rules and targets
#
################################################################################

flags:	
	@echo "NVCCFLAGS=$(NVCCFLAGS)"
	@echo ""
	@echo "CFLAGS=$(CFLAGS)"
	@echo ""
	@echo "CXXFLAGS=$(CXXFLAGS)"

Engine.a:
	@cd Engine; $(MAKE)
	
Engine.a.clean:
	@cd Engine; $(MAKE) clean
	
Algorithms.a:
	@cd Algorithms; $(MAKE)
	
Algorithms.a.clean:
	@cd Algorithms; $(MAKE) clean
	
php: $(TARGET)
	@echo "[CP]	$(TARGET) -> /usr/local/lib"
	$(VERBOSE)cp  $(TARGET) /usr/local/lib
	@cd php; $(MAKE)
	
php.clean::
	@cd php; $(MAKE) clean

run: $(TARGET)
	$(TARGET)

tests:
	$(eval export CFLAGS := -DTESTS)
	$(eval export CXXFLAGS := -DTESTS)
	@$(MAKE)
	@$(TARGET)
