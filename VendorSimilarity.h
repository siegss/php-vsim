/*
 * VendorSimilarity.h
 *
 *  Created on: Jul 9, 2011
 *      Author: markhor
 */

#ifndef VENDORSIMILARITY_H_
#define VENDORSIMILARITY_H_
#include <stdint.h>

typedef struct {
	char *name;
	uint32_t id;
	float similarity;
} VendorData;

typedef struct {
	VendorData *vendors;
	uint32_t count;
} VendorList;

void SimilarVendorsInitialize(void);

void SimilarVendorsFinalize(void);

VendorList FindSimilarVendors(const char *vendor);

VendorData GetVendorFromList(VendorList list, uint32_t index);

uint32_t GetVendorListLength(VendorList list);

void FreeVendorData(VendorList list);

#endif /* VENDORSIMILARITY_H_ */
