#include <stdio.h>
#include "VendorSimilarity.h"


int main()
{
	// Initialize the library
	SimilarVendorsInitialize();

	// Find the matches
	VendorList list;	
	list = FindSimilarVendors("cocacola");		
	uint32_t foundLength = GetVendorListLength(list);
	if (foundLength < 0) {
		printf("An error happened\n");
		return -1;
	}
	// Output the matches
	for (uint32_t i = 0; i < foundLength; i++) {
		VendorData vd = GetVendorFromList(list, i);
		printf("%f\t[%u]\t%s\n", vd.similarity, vd.id, vd.name);
	}
	// Free the results
	FreeVendorData(list);
	// Free the library
	SimilarVendorsFinalize();
	return 0;
}
