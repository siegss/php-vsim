/*
 * MySQLConnector.h
 *
 *  Created on: May 11, 2011
 *      Author: markhor
 */

#ifndef MYSQLCONNECTOR_H_
#define MYSQLCONNECTOR_H_

#define HOST      "mysql02"
#define PORT      3306
#define PORTSTR   "3306"
#define USERNAME  "cstore2user"
#define PASSWORD  "cstore2pwd"
#define DBNAME    "gatekeeper"

// Converts the dump made by mysqldump to the internal representation
int PreprocessDump(const char *sqlFile, const char *dumpFile);

// Executes the specified query, saving the result to the specified file.
// Opens and closes MySQL connection automatically.
int DumpTable(const char *sql, const char *file);

#endif /* MYSQLCONNECTOR_H_ */
