/*
 * GlobalUtils.h
 *
 *  Created on: May 4, 2011
 *      Author: markhor
 */

#ifndef GLOBALUTILS_H_
#define GLOBALUTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define LIKELY(x) __builtin_expect(x, true)
#define UNLIKELY(x) __builtin_expect(x, false)

#define CONTEXT "[%s, %s:%d]\n"
#define WARNING(mask, ...) \
	fprintf (stderr, CONTEXT "Warning: " mask "\n", __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
#define ERROR(mask, ...) \
	do { fprintf (stderr, CONTEXT "Error: " mask "\n", __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__); \
	  	 perror(0); \
	  	 exit(-1);  } while (0)

#define ALLOCATED_POINTER_CHECK(ptr) if (!ptr) { \
	ERROR("%s", "Failed to allocate memory"); \
	exit(-1); \
}

#endif /* GLOBALUTILS_H_ */
