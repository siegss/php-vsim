#ifndef STRING_DISTANCE_H
#define STRING_DISTANCE_H

#define __STDC_LIMIT_MACROS
#include <stdint.h>
#include <malloc.h>
#include <string.h>

#define THRESHOLD 0.749f

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CommonSuffixStruct {
	const char *Suffix;
	uint8_t SuffixLength;
	float Importance;
	int SynonymGroup;

#ifdef __cplusplus
	CommonSuffixStruct(const char *name, float importance, int group) :
		Suffix(name), SuffixLength(strlen(name)), Importance(importance), SynonymGroup(group) {}
#endif
} CommonSuffix;


void InitializeCommonSuffixes(void);
void FreeCommonSuffixes(void);

uint8_t StringDistance(const char *s1, uint8_t length1,
	const char *s2, uint8_t length2);

float NormalizedStringSimilarity(const char *s1, const char *s2);

void PreprocessVendorName(const char *vendor, char **str, CommonSuffix **symanticParts, uint8_t *partsCount);

float VendorStringDistance(const char *s1, const char *s2,
		const CommonSuffix *symanticParts1, const CommonSuffix *symanticParts2,
		uint8_t partsCount1, uint8_t partsCount2);

float VendorStringDistancePreprocessed(const char *s1, const char *s2);

#ifdef __cplusplus
}
#endif

#endif // STRING_DISTANCE_H
