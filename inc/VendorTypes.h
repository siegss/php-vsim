/*
 * VendorTypes.h
 *
 *  Created on: Jul 9, 2011
 *      Author: markhor
 */

#ifndef VENDORTYPES_H_
#define VENDORTYPES_H_

#include "StringDistance.h"

typedef struct _VendorPair {
	int Id;
	char *Name;
	const CommonSuffix *Suffixes;
	uint8_t SuffixCount;
#ifdef __cplusplus
	_VendorPair(int id, char *name) :
		Id(id), Name(name), Suffixes(0), SuffixCount(0) {}
	_VendorPair(int id, char *name, const CommonSuffix *suffixes, uint8_t suffixCount) :
		Id(id), Name(name), Suffixes(suffixes), SuffixCount(suffixCount) {}
#endif
} VendorPair;

#endif /* VENDORTYPES_H_ */
