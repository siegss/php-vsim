// StringDistance.cpp : Defines the entry point for the console application.

#include "StringDistance.h"
#include "GlobalUtils.h"
#include <ctype.h>
#include <vector>

CommonSuffix *Suffixes = 0;
#define SUFFIXES_COUNT 64
#define ADD_SUFFIX(s, i) Suffixes[index++] = CommonSuffix(s, i, group)
#define ADD_UNIQUE_SUFFIX(s, i) Suffixes[index++] = CommonSuffix(s, i, -1)

extern "C" {

void InitializeCommonSuffixes()
{
	assert(!Suffixes && "Already initialized");
	Suffixes = (CommonSuffix *) malloc(SUFFIXES_COUNT * sizeof(CommonSuffix));
	int index = 0;
	int group = 0;
	// The values below are hardcoded using the frequency analysis of sample vendor names list
	// The frequency analysis was conducted using "wf" program (http://async.com.br/~marcelo/wf)
	ADD_SUFFIX("inc",			0.125);
	ADD_SUFFIX("incorporated",	0.75);
	ADD_SUFFIX("corp",			0.5);
	ADD_SUFFIX("corporation",	0.5);
	ADD_SUFFIX("corporate",		0.75);
	group++;
	ADD_SUFFIX("co",			0.125);
	ADD_SUFFIX("company",		0.125);
	group++;
	ADD_SUFFIX("distributing",	0.125);
	ADD_SUFFIX("distributors",	0.125);
	ADD_SUFFIX("dist",			0.25);
	ADD_SUFFIX("distribution",	0.25);
	ADD_SUFFIX("distributor",	0.5);
	ADD_SUFFIX("distr",			0.75);
	ADD_SUFFIX("distributers",	0.75);
	ADD_SUFFIX("distibutors",	0.75);
	ADD_SUFFIX("distrib",		0.75);
	ADD_SUFFIX("distribution",	0.75);
	ADD_SUFFIX("distribuidora",	0.8);
	group++;
	ADD_SUFFIX("llc",			0.125);
	ADD_SUFFIX("limited",		0.125);
	ADD_SUFFIX("ltd",			0.125);
	group++;
	ADD_UNIQUE_SUFFIX("wholesale",	0.125);
	ADD_SUFFIX("beverage",		0.25);
	ADD_SUFFIX("beverages",		0.5);
	group++;
	ADD_SUFFIX("foods",			0.25);
	ADD_SUFFIX("food",			0.25);
	group++;
	ADD_SUFFIX("service",		0.25);
	ADD_SUFFIX("services",		0.25);
	group++;
	ADD_UNIQUE_SUFFIX("lottery",0.25);
	ADD_UNIQUE_SUFFIX("sales",	0.25);
	ADD_SUFFIX("enterprises",	0.25);
	ADD_SUFFIX("enterprise",	0.75);
	group++;
	ADD_UNIQUE_SUFFIX("dairy",	0.5);
	ADD_UNIQUE_SUFFIX("coffee",	0.5);
	ADD_SUFFIX("gas",			0.5);
	ADD_SUFFIX("petroleum",		0.5);
	ADD_SUFFIX("gasoline",		0.75);
	group++;
	ADD_UNIQUE_SUFFIX("bakery",	0.5);
	ADD_UNIQUE_SUFFIX("bottling",	0.5);
	ADD_UNIQUE_SUFFIX("products",	0.5);
	ADD_SUFFIX("store",			0.5);
	ADD_SUFFIX("stores",		0.75);
	ADD_SUFFIX("storage",		0.75);
	group++;
	ADD_UNIQUE_SUFFIX("group",	0.5);
	ADD_UNIQUE_SUFFIX("state",	0.5);
	ADD_UNIQUE_SUFFIX("marketing",	0.5);
	ADD_UNIQUE_SUFFIX("brands",	0.5);
	ADD_UNIQUE_SUFFIX("farms",	0.5);
	ADD_UNIQUE_SUFFIX("national",	0.5);
	ADD_UNIQUE_SUFFIX("tobacco",	0.5);
	ADD_SUFFIX("acc",			0.5);
	ADD_SUFFIX("accounts",		0.75);
	group++;
	ADD_SUFFIX("supply",		0.5);
	ADD_SUFFIX("supplies",		0.75);
	group++;
	ADD_UNIQUE_SUFFIX("international",	0.75);
	ADD_UNIQUE_SUFFIX("trading",	0.75);
	ADD_UNIQUE_SUFFIX("farm",		0.75);
	ADD_UNIQUE_SUFFIX("produce",	0.75);
	ADD_UNIQUE_SUFFIX("systems",	0.75);
	ADD_SUFFIX("industries",	0.75);
	ADD_SUFFIX("industrial",	0.75);
	group++;
	ADD_SUFFIX("communications",0.75);
	ADD_SUFFIX("comm",			0.75);
	group++;
	ADD_UNIQUE_SUFFIX("solutions",	0.75);
}

void FreeCommonSuffixes()
{
	free(Suffixes);
	Suffixes = 0;
}

// Simplified Damerau-Levenstein distance with linear memory consumption
// This is a modified Vagner-Fisher algorithm
uint8_t StringDistance(const char *s1, uint8_t length1,
	const char *s2, uint8_t length2)
{	
	assert(length1 < UINT8_MAX && length2 < UINT8_MAX);
	const uint8_t step = length2 + 1;
	// Unknown row, previous row, the row before previous row
	uint8_t matrix[(length2 + 1) * 3];
	
	for (uint8_t i = 0; i < step; i++) {
		matrix[2 * step + i] = i;
	}
	for (uint8_t i = 1; i <= length1; i++) {
		// Define the real indices
		uint8_t ind1 = i != 2? (i - 3) % 3 : 2;
		uint8_t ind2 = i != 1? (i - 2) % 3 : 2;
		uint8_t ind3 = (i - 1) % 3;
		matrix[ind3 * step] = i;
		// Calculate the row
		for (uint8_t j = 1; j <= length2; j++) {
			uint8_t cost = s1[i - 1] == s2[j - 1]? 0 : 1;
			uint8_t del = matrix[ind2 * step + j] + 1;
			uint8_t ins = matrix[ind3 * step + j - 1] + 1;
			uint8_t sub = matrix[ind2 * step + j - 1] + cost;
			uint8_t min = del > ins? ins : del;
			if (sub < min) min = sub;
			if (LIKELY(i > 1 && j > 1) && UNLIKELY(s1[i - 1] == s2[j - 2] && s1[i - 2] == s2[j - 1])) {
				uint8_t tra = matrix[ind1 * step + j - 2] + cost;
				if (tra < min) min = tra;
			}

			matrix[ind3 * step + j] = min;
		}
	}
	return matrix[((length1 - 1) % 3) * step + length2];
}

float NormalizedStringSimilarity(const char *s1, const char *s2)
{
	int length1 = strlen(s1);
	int length2 = strlen(s2);
	if (length1 == 0 || length2 == 0) {
		return 0.0f;
	}
	uint8_t dist = StringDistance(s1, length1, s2, length2);
	uint8_t minlength = length1 > length2? length2 : length1;
	float result = 1.0f - (dist + 0.0f) / minlength;
	if (result < 0) return 0;
	return result;
}

static inline bool EndsWith(const char *str, int length, const char *end)
{
	int endLength = strlen(end);
	return strcmp(str + length - endLength - 1, end) == 0;
}

#define DISCARD_ENDING(str, length, end) if (EndsWith(str, length, end)) { \
	length -= strlen(end); \
	str[length - 1] = 0; \
}

#define STANDARDIZE_ENDING(str, length, end, standard) if (EndsWith(str, length, end)) { \
	DISCARD_ENDING(str, length, end) \
	strcpy(str + length, standard); \
	length += strlen(standard); \
}

#define DISCARD_ENDINGS(str, len) DISCARD_ENDING(str, len, "enterprises") \
		DISCARD_ENDING(str, len, "incorporated") \
		DISCARD_ENDING(str, len, "incorp") \
		DISCARD_ENDING(str, len, "inc") \
		DISCARD_ENDING(str, len, "limited") \
		DISCARD_ENDING(str, len, "lim") \
		DISCARD_ENDING(str, len, "ltd") \
		DISCARD_ENDING(str, len, "llc") \
		STANDARDIZE_ENDING(str, len, "distributing", "dist") \
		STANDARDIZE_ENDING(str, len, "distributors", "dist") \
		STANDARDIZE_ENDING(str, len, "distribution", "dist") \
		STANDARDIZE_ENDING(str, len, "distr", "dist") \
		DISCARD_ENDING(str, len, "co") \
		DISCARD_ENDING(str, len, "corp") \
		DISCARD_ENDING(str, len, "corporation") \
		DISCARD_ENDING(str, len, "corporations") \
		DISCARD_ENDING(str, len, "company") \
		DISCARD_ENDING(str, len, "wholesale") \
		DISCARD_ENDING(str, len, "sales") \
		DISCARD_ENDING(str, len, "communications") \
		DISCARD_ENDING(str, len, "produce") \
		STANDARDIZE_ENDING(str, len, "beverages", "beverage")

#define CHECK_SYMBOL_RANGE(ch) (ch == ' ' || ch == '-' || ch == '&' || ch == '\'' || ch == '.' || ch == ',')

void PreprocessVendorName(const char *vendor, char **str, CommonSuffix **symanticParts, uint8_t *partsCount)
{
	// Allocate buffer, convert to lower case
	int l = strlen(vendor);	
	char rs[l];
	for (int i = 0; i < l; i++) {
		rs[i] = tolower(vendor[i]);
	}
	
	// Trim
	int pos, end;
	for (pos = 0; (rs[pos] < 'a' || rs[pos] > 'z') && pos < l; pos++);	
	for (end = l - 1; (rs[end] < 'a' || rs[end] > 'z') && end >= 0; end--);
	l = end + 1;
		
	// Remove the leading "the"
	if (strncmp(rs, "the", 3) == 0) {
		pos += 3;
		while (rs[pos] == ' ') pos++;
	}

	// Replace "and" with ampersand
	for (int i = pos; i < l; i++) {
		if (strncmp(rs + i, " and ", 5) == 0) {
			rs[i + 1] = ' ';
			rs[i + 2] = '&';
			rs[i + 3] = ' ';
			i += 5;
		}
	}	

	// Collapse spaces, dashes, ampersand, single quotes, dots and commas
	int rl = l - pos;	
	for (int i = pos; i < l; i++) {
		if (CHECK_SYMBOL_RANGE(rs[i])) rl--;
	}	
	char vs[++rl];
	for (int i = pos, idx = 0; i < l; i++) {
		if (!CHECK_SYMBOL_RANGE(rs[i])) {
			vs[idx++] = rs[i];
		}
	}	
	vs[rl - 1] = 0;

	// Now vs is the preprocessed vendor name
	// rl is the length (including ending zero)
	
	// Try to discard some common endings
	std::vector<CommonSuffix *> positions;
	uint8_t prevPos = rl - 1;
	for (pos = rl - 3; pos > 0; pos--) {
		int i;
		for (i = 0; i < SUFFIXES_COUNT; i++) {
			if (LIKELY(prevPos - pos != Suffixes[i].SuffixLength)) continue;
			if (UNLIKELY(!strncmp(vs + pos, Suffixes[i].Suffix, prevPos - pos))) break;
		}
		if (i < SUFFIXES_COUNT) {
			positions.push_back(Suffixes + i);
			prevPos = pos;
		}
	}
	*partsCount = positions.size();
	if (positions.size() > 0) {
		*symanticParts = (CommonSuffix *) malloc(sizeof(CommonSuffix) * positions.size());
		uint8_t idx = 0;
		for (std::vector<CommonSuffix *>::iterator I = positions.begin(), E = positions.end(); I != E; I++) {
			(*symanticParts)[idx++] = **I;
		}
	} else {
		*symanticParts = 0;
	}
	// Finish
	*str = (char *) malloc(prevPos + 1);
	strncpy(*str, vs, prevPos);
	(*str)[prevPos] = 0;
	//printf("%s -> %s\n", vendor, *str);
}

float VendorStringDistance(const char *s1, const char *s2,
		const CommonSuffix *symanticParts1, const CommonSuffix *symanticParts2,
		uint8_t partsCount1, uint8_t partsCount2)
{
	// We've done?
	if (strcmp(s1, s2) == 0) {
		if (strlen(s1) == 0) {
			return 0.0f;
		}
		else {
			return 1.0f;
		}
	}

	// Do the advanced stuff
	// Firstly, calculate the core similarity
	float similarity = NormalizedStringSimilarity(s1, s2);
	if (partsCount1 == 0 && partsCount2 == 0) {
		return similarity;
	}
	// Deal with some common cases
	if (partsCount1 == 0) {
		for (uint8_t j = 0; j < partsCount2; j++) {
			similarity -= symanticParts2[j].Importance;
		}
		return similarity;
	}
	if (partsCount2 == 0) {
		for (uint8_t j = 0; j < partsCount1; j++) {
			similarity -= symanticParts1[j].Importance;
		}
		return similarity;
	}
	// Now consider the suffixes
	for (uint8_t i = 0; i < partsCount1; i++) {
		bool found = false;
		for (uint8_t j = 0; j < partsCount2; j++) {
			if ((symanticParts1[i].SuffixLength == symanticParts2[j].SuffixLength &&
				!strcmp(symanticParts1[i].Suffix, symanticParts2[j].Suffix)) ||
				(symanticParts1[i].SynonymGroup == symanticParts2[j].SynonymGroup &&
				symanticParts1[i].SynonymGroup != -1)) {
				found = true;
				break;
			}
		}
		if (!found) {
			similarity -= symanticParts1[i].Importance;
		}
	}
	// Symmetrically repeat
	for (uint8_t i = 0; i < partsCount2; i++) {
		bool found = false;
		for (uint8_t j = 0; j < partsCount1; j++) {
			if ((symanticParts2[i].SuffixLength == symanticParts1[j].SuffixLength &&
				!strcmp(symanticParts2[i].Suffix, symanticParts1[j].Suffix)) ||
				symanticParts2[i].SynonymGroup == symanticParts1[j].SynonymGroup) {
				found = true;
				break;
			}
		}
		if (!found) {
			similarity -= symanticParts2[i].Importance;
		}
	}
	if (similarity < 0) return 0.0f;
	return similarity;
}

float VendorStringDistancePreprocessed(const char *s1, const char *s2)
{
	char *vs1, *vs2;
	CommonSuffix *symParts1, *symParts2;
	uint8_t partsCount1, partsCount2;
	PreprocessVendorName(s1, &vs1, &symParts1, &partsCount1);
	PreprocessVendorName(s2, &vs2, &symParts2, &partsCount2);
	float res = VendorStringDistance(vs1, vs2, symParts1, symParts2, partsCount1, partsCount2);
	free(vs1);
	free(vs2);
	free(symParts1);
	free(symParts2);
	return res;
}

}
