// VendorCleaner.cpp : Defines the entry point for the console application.
//

#include "StringDistance.h"
#include "VendorTypes.h"
#include "GlobalUtils.h"
extern "C" {
#include "MySQLConnector.h"
}
#include <vector>
//#include <map>
//#include <string>
#include <iostream>
#include <fstream>
#include <string.h>

#define LINE_LENGTH 128
using namespace std;

// Legacy stuff
/*

#define OUTPUT_FILE "/tmp/SimilarVendors.sql"

#define TEST_TRIVIAL(s1, s2) res = VendorStringDistancePreprocessed(s1, s2); \
	if (res > 0.99f) { \
		printf("OK\t\"%s\" <=> \"%s\"\n", s1, s2); \
	} else { \
		printf("FAIL\t\"%s\" <=> \"%s\" (%f similarity)\n", s1, s2, res); \
		failed = true; \
	}

#define TEST(s1, s2) res = VendorStringDistancePreprocessed(s1, s2); \
	if (res > THRESHOLD) { \
		printf("OK\t\"%s\" <=> \"%s\"\n", s1, s2); \
	} else { \
		printf("FAIL\t\"%s\" <=> \"%s\" (%f similarity)\n", s1, s2, res); \
		failed = true; \
	}

#define TEST_NOT(s1, s2) res = VendorStringDistancePreprocessed(s1, s2); \
	if (res < THRESHOLD) { \
		printf("OK\t\"%s\" <=> \"%s\"\n", s1, s2); \
	} else { \
		printf("FAIL\t\"%s\" <=> \"%s\" (%f similarity)\n", s1, s2, res); \
		failed = true; \
	}

void dfs1 (int v, vector<uint8_t> &used, vector<int> &order, vector<vector<int> > &g)
{
	used[v] = true;
	for (size_t i=0; i<g[v].size(); ++i)
	if (!used[ g[v][i] ])
		dfs1 (g[v][i], used, order, g);
	order.push_back (v);
}

void dfs2 (int v, vector<uint8_t> &used, vector<int> &component, vector<vector<int> > &g)
{
	used[v] = true;
	component.push_back (v);
	for (size_t i=0; i<g[v].size(); ++i)
		if (!used[ g[v][i] ])
			dfs2 (g[v][i], used, component, g);
}

void ProduceSimilarVendors()
{
	InitializeCommonSuffixes();
	// First do some integrity tests
	float res;
	bool failed = false;
	TEST_TRIVIAL("AT & T", "AT&T")
	TEST_TRIVIAL("AT & T", "At&t")
	TEST_TRIVIAL("AT & T", "(7492) AT&T (54)")
	TEST_TRIVIAL("AT & T", "AT&T INC")
	TEST_TRIVIAL("AT & T", "AT T INC.")
	TEST_TRIVIAL("AT & T", "AT and T Limited")
	TEST("ALMANAC", "Almanah")
	TEST("altoona store bar expense", "altoonabp store expense")
	TEST_NOT("ACADIAN BAKERY", "Acadiana Wholesale")
	TEST_NOT("001. CARDIN DISTRIBUTING CO.", "CAFFEY DISTRIBUTING CO.")
	TEST_NOT("9278 Communications, Inc.", "Distribution INc")
	TEST("REPUBLIC BEVERAGE", "Republic")

	if (failed) {
		printf("\n\nOne or more tests were failed. Unable to continue.\n");
		return -1;
	}

	printf("\nRetrieving the dump...\n");
	DumpTable("select Id, Name from gatekeeper.GlobalVendors", "/tmp/Vendors.dat");

	printf("Parsing the dump...\n");
	vector<VendorPair> data, orig;
	{
		ifstream input("/tmp/Vendors.dat");
		char line[LINE_LENGTH];
		int id;
		input.getline(line, LINE_LENGTH);
		while (!input.eof()) {
			char *str = (char *) malloc(LINE_LENGTH);
			int pr = sscanf(line, "%u,%[^\"]", &id, str);
			assert(pr == 2 && "Encountered a line which could not be parsed");
			char *vs;
			CommonSuffix *symParts;
			uint8_t partsLength;
			PreprocessVendorName(str, &vs, &symParts, &partsLength);
			data.push_back(VendorPair(id, vs, symParts, partsLength));
			orig.push_back(VendorPair(id, str));
			input.getline(line, LINE_LENGTH);
		}
		input.close();
	}
	printf("Comparing strings...\n");
	std::map<std::string, uint16_t> trivialDuplicates;
	uint8_t *matrix = (uint8_t *) malloc(data.size() * data.size());
	memset(matrix, 0, data.size() * data.size());
	for (int i = 0; i < (int)data.size(); i++) {
		if (trivialDuplicates.count(data[i].Name) == 0) {
			trivialDuplicates.insert(std::pair<std::string, uint16_t>(data[i].Name, 1));
		} else {
			trivialDuplicates[data[i].Name]++;
		}
	}
	#pragma omp parallel for
	for (int i = 0; i < (int)data.size(); i++) {
		matrix[i * data.size() + i] = 1;
		for (size_t j = i + 1; j < data.size(); j++) {
			float sim = -1.0f;
			if (strcmp(data[i].Name, data[j].Name)) {
				#pragma omp critical
				if (trivialDuplicates[data[i].Name] > 2 && trivialDuplicates[data[j].Name] > 2) {
					sim = 0.0f;
				}
			}
			if (sim < 0) {
				sim = VendorStringDistance(data[i].Name, data[j].Name,
					data[i].Suffixes, data[j].Suffixes, data[i].SuffixCount, data[j].SuffixCount);
			}
			uint8_t val = sim > THRESHOLD? 1 : 0;
			matrix[i * data.size() + j] = val;		
			matrix[j * data.size() + i] = val;
		}
	}
	// Build the graph
	printf("Building the graph...\n");
	vector<vector<int> > graph;
	for (size_t i = 0; i < data.size(); i++) {
		graph.push_back(vector<int>());
		for (size_t j = 0; j < data.size(); j++) {
			if (matrix[i * data.size() + j] == 1 && i != j) {
				graph[i].push_back(j);
			}
		}
	}

	// Now we need to determine the groups in which everybody is similar to the other
	// In other words, to find all complete subgraphs (http://en.wikipedia.org/wiki/Complete_graph)
	// This problem is called the Clique problem (http://en.wikipedia.org/wiki/Clique_problem)
	// It is NP-complete; I propose the following approach:
	// 1 - find all the connected components (http://en.wikipedia.org/wiki/Connected_component_(graph_theory))
	// 2 - deal with each component independently using a simple heuristic:
	//     if a node has the number of connections lower than average, throw it away.
	//     It does not guarantee the precise results, but we do not need them.

	// First step - perform graph condensation
	// Source: e-maxx algo p.68 http://e-maxx.ru/upload/e-maxx_algo.pdf
	printf("Condensing the similarity graph...\n");
	vector<vector<int> > similarVendorsSurrogate;
	{
		vector<uint8_t> used;
		vector<int> order, component;
		used.assign (data.size(), false);
		for (size_t i = 0; i < data.size(); i++)
			if (!used[i])
				dfs1 (i, used, order, graph);
		used.assign (data.size(), false);
		for (size_t i = 0; i < data.size(); i++) {
			int v = order[data.size() - 1 - i];
				if (!used[v]) {
					dfs2 (v, used, component, graph);
					similarVendorsSurrogate.push_back(component);
					component.clear();
				}
		}
	}

	printf("Cleaning up...\n");
	remove("/tmp/Vendors.dat");
	free(matrix);
	for (vector<VendorPair>::iterator I = data.begin(), E = data.end(); I != E; I++) {
		free(I->Name);
		free((void *)I->Suffixes);
	}
	for (vector<VendorPair>::iterator I = orig.begin(), E = orig.end(); I != E; I++) {
		free(I->Name);
	}

	// Second step - throw away vendors with the number of connections fewer than average
	printf("Splitting into complete subgraphs...\n");
	vector<vector<int> > similarVendors;
	similarVendors.reserve(similarVendorsSurrogate.size());
	for (vector<vector<int> >::iterator I = similarVendorsSurrogate.begin(), E = similarVendorsSurrogate.end(); I != E; I++) {
		uint32_t connectionsSum = 0;
		for (vector<int>::iterator GI = I->begin(), GE = I->end(); GI != GE; GI++) {
			connectionsSum += graph[*GI].size();
		}
		uint32_t avg;
		if (connectionsSum % I->size() > 0) {
			avg = connectionsSum / I->size();
		} else {
			avg = connectionsSum / I->size() - 1;
		}
		similarVendors.push_back(std::vector<int>());
		for (vector<int>::iterator GI = I->begin(), GE = I->end(); GI != GE; GI++) {
			if (graph[*GI].size() > avg) {
				similarVendors.back().push_back(*GI);
			}
		}
	}
	similarVendorsSurrogate.clear();
	graph.clear();

	int similarityPower = 0, similarityGroups = 0;
	printf("Writing the results...\n");
	{
		ofstream output(OUTPUT_FILE);
		output << "INSERT INTO " DBNAME ".similarvendors VALUES ";
		for (vector<vector<int> >::iterator I = similarVendors.begin(), E = similarVendors.end(); I != E; I++) {
			if (I->size() > 1) {
				similarityGroups++;
				similarityPower += I->size();
				for (size_t i = 0; i < I->size(); i++) {
					for (size_t j = 0; j < I->size(); j++) {
						if (UNLIKELY(I + 1 == E && i == I->size() - 1 && j == I->size() - 2)) {
							output << "(" << orig[I->at(i)].Id << "," << orig[I->at(j)].Id << ");";
						} else {
							if (i != j) {
								output << "(" << orig[I->at(i)].Id << "," << orig[I->at(j)].Id << "),";
							}
						}
					}
				}
			}
		}
		output.close();
	}
	printf("Found %i groups of similar vendors\nOverall count: %i\n", similarityGroups, similarityPower);
	// select b.Name, c.Name from gatekeeper.similarvendors a inner join gatekeeper.GlobalVendors b on a.vendorid1 = b.Id inner join gatekeeper.GlobalVendors c on a.vendorid2 = c.Id
	printf("Uploading the results to the MySQL server...\n");
	if (system("mysql -C --host=" HOST " --port=" PORTSTR " --user=" USERNAME
		   " --password=" PASSWORD " -e \"truncate table " DBNAME ".similarvendors; "
		   "source " OUTPUT_FILE ";\"")) {
		printf("We failed. \"" OUTPUT_FILE "\" is left on the disk\n");
	} else {
		remove(OUTPUT_FILE);
		printf("Success\n");
	}
}
*/

#define AUX_FILE "/tmp/Vendors.dat"

extern "C" {
void InternalInitialize(VendorPair **preprocessedVendors, VendorPair **originalVendors, uint32_t *count)
{
	InitializeCommonSuffixes();
	//printf("\nRetrieving the dump...\n");
	DumpTable("select Id, Name from gatekeeper.GlobalVendors", AUX_FILE);

	//printf("Parsing the dump...\n");
	vector<VendorPair> data, orig;
	{
		ifstream input("/tmp/Vendors.dat");
		char line[LINE_LENGTH];
		int id;
		input.getline(line, LINE_LENGTH);
		while (!input.eof()) {
			char *str = (char *) malloc(LINE_LENGTH);
			int pr = sscanf(line, "%u,%[^\"]", &id, str);
			assert(pr == 2 && "Encountered a line which could not be parsed");
			char *vs;
			CommonSuffix *symParts;
			uint8_t partsLength;
			PreprocessVendorName(str, &vs, &symParts, &partsLength);
			data.push_back(VendorPair(id, vs, symParts, partsLength));
			orig.push_back(VendorPair(id, str));
			input.getline(line, LINE_LENGTH);
		}
		input.close();
	}

	remove(AUX_FILE);

	*preprocessedVendors = (VendorPair *) malloc(sizeof(VendorPair) * data.size());
	ALLOCATED_POINTER_CHECK(*preprocessedVendors);
	*originalVendors = (VendorPair *) malloc(sizeof(VendorPair) * data.size());
	ALLOCATED_POINTER_CHECK(*originalVendors);
	for (uint32_t i = 0; i < data.size(); i++) {
		(*preprocessedVendors)[i] = data[i];
		(*originalVendors)[i] = orig[i];
	}
	*count = data.size();
}
}
