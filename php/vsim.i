%module vsim
%{
typedef struct {
	char *name;
	unsigned id;
	float similarity;
} VendorData;

typedef struct {
	VendorData *vendors;
	unsigned count;
} VendorList;

void SimilarVendorsInitialize(void);

void SimilarVendorsFinalize(void);

VendorList FindSimilarVendors(const char *vendor);

VendorData GetVendorFromList(VendorList list, unsigned index);

unsigned GetVendorListLength(VendorList list);

void FreeVendorData(VendorList list);
%}

typedef struct {
        char *name;
        unsigned id;
        float similarity;
} VendorData;

typedef struct {
        VendorData *vendors;
        unsigned count;
} VendorList;

void SimilarVendorsInitialize(void);

void SimilarVendorsFinalize(void);

VendorList FindSimilarVendors(const char *vendor);

VendorData GetVendorFromList(VendorList list, unsigned index);

unsigned GetVendorListLength(VendorList list);

void FreeVendorData(VendorList list);

