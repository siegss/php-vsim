/*
 * MySQLConnector.c
 *
 *  Created on: May 11, 2011
 *      Author: markhor
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <alloca.h>
#include "mysql.h"
#include "MySQLConnector.h"
#include "GlobalUtils.h"

// SSH tunnel:
// ssh -L 13308:mysql02:3306 gentoo11.petrosoftinc.com

static inline void MySqlError(const char *sql, const char *file, MYSQL *mysql)
{
	ERROR("Failed to execute \"%s\", saving the result to %s\n%s\n", sql, file, mysql_error(mysql));
}

#define DUMP_TABLE_ERROR() { MySqlError(sql, file, &mysql); return -1; }
#define DELIMITER ","

/*
 sed -e '/),/,/ (/ !d' -e 's/INSERT INTO .* VALUES //' -e 's/),(/\n/g' -e "s/,'/,/g" -e "s/',/,/g" -e 's/);$//g' -e 's/^ (//g' -e 's/),$//g' -e 's/^(//g'
 */

int PreprocessDump(const char *sqlFile, const char *dumpFile)
{
	assert (sqlFile && dumpFile && "Null pointer was passed!");
	const char *sed = "sed -e '/),/,/ (/ !d' -e 's/INSERT INTO .* VALUES //' "
					  "-e 's/),(/\\n/g' -e \"s/,'/,/g\" -e \"s/',/,/g\" -e 's/);$//g' -e 's/^ (//g' -e 's/),$//g' -e 's/^(//g'";
	char *cmd = (char *)alloca(strlen(sed) + strlen(sqlFile) + strlen(dumpFile) + 4);
	strcpy(cmd, sed);
	strcat(cmd, sqlFile);
	strcat(cmd, " > ");
	strcat(cmd, dumpFile);
	return(system(cmd));
}

int DumpTable(const char *sql, const char *file)
{
	assert (sql && file && "Null pointer was passed!");
	MYSQL mysql;
	MYSQL_RES *res;
	MYSQL_ROW row;
	mysql_init(&mysql);
	if (!mysql_real_connect(&mysql, HOST, USERNAME, PASSWORD, DBNAME, PORT, 0, CLIENT_COMPRESS)) {
		DUMP_TABLE_ERROR()
	}
	if (mysql_query(&mysql, sql)) {
		DUMP_TABLE_ERROR()
	}
	if (!(res = mysql_store_result(&mysql))) {
		DUMP_TABLE_ERROR()
	}
	FILE *fw = fopen(file, "w");
	if (!fw) {
		DUMP_TABLE_ERROR()
	}
	while((row = mysql_fetch_row(res))) {
		unsigned int num_fields = mysql_num_fields(res);
		for (unsigned int i = 0 ; i < num_fields; i++) {
			if (!row[i]) continue;
			size_t length = strlen(row[i]);
			if (fwrite(row[i], 1, length, fw) != length) ERROR("Failed to write the dump to \"%s\"", file);
			if (i < num_fields - 1) {
				if (fwrite(DELIMITER, 1, 1, fw) != 1) ERROR("Failed to write the dump to \"%s\"", file);
			}
		}
		if (fwrite("\n", 1, 1, fw) != 1) ERROR("Failed to write the dump to \"%s\"", file);
	}
	fclose(fw);
	if (!mysql_eof(res)) {
		DUMP_TABLE_ERROR()
	}
	mysql_free_result(res);
	mysql_close(&mysql);
	return 0;
}
