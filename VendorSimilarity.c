/*
 * VendorSimilarity.c
 *
 *  Created on: Jul 9, 2011
 *      Author: markhor
 */

#include "VendorSimilarity.h"
#include "VendorTypes.h"
#include "GlobalUtils.h"
#include "StringDistance.h"
#include <pthread.h>
#define __USE_XOPEN_EXTENDED
#include <unistd.h>

int Users = -1;

#define PTHREAD_INVOKE(func, funcStr, ...) do { \
	if ((res = func(__VA_ARGS__))) { \
		ERROR("failed to initialize the library. " funcStr "() returned %i", res); \
	} } while (0)

int mutexInitialized = 0;
pthread_mutex_t lock;

VendorPair *preprocessedVendors = 0;
VendorPair *originalVendors = 0;
uint32_t registeredVendorsCount = 0;

extern void InternalInitialize(VendorPair **, VendorPair **, uint32_t *);

// Download and preprocess the vendors in the database
void SimilarVendorsInitialize()
{
	int res;
	if (!mutexInitialized) {
		PTHREAD_INVOKE(pthread_mutex_init, "pthread_mutex_init", &lock, 0);
		mutexInitialized = 1;
	}
	PTHREAD_INVOKE(pthread_mutex_lock, "pthread_mutex_lock", &lock);
	InternalInitialize(&preprocessedVendors, &originalVendors, &registeredVendorsCount);
	Users = 0;
	PTHREAD_INVOKE(pthread_mutex_unlock, "pthread_mutex_unlock", &lock);	
}

static int CompareFuncForQSortInFindSimilarVendors(const void *p1, const void *p2)
{
	return ((VendorData *)p1)->similarity < ((VendorData *)p2)->similarity;
}

VendorList FindSimilarVendors(const char *vendor)
{	
	VendorList err;
	err.count = 0;
	err.vendors = 0;
	if (!vendor) return err;
	int res;
	// FindSimilarVendors is thread safe; we acquire the lock to ensure the library is initialized
	// and increment the number of active calls by 1
	PTHREAD_INVOKE(pthread_mutex_lock, "pthread_mutex_lock", &lock);
	if (Users == -1) {
		ERROR("%s", "the library was not initialized");
		return err;
	}
	Users++;
	PTHREAD_INVOKE(pthread_mutex_unlock, "pthread_mutex_unlock", &lock);
	char *core;
	CommonSuffix *parts;
	uint8_t partsLength;
	// Preprocess the given vendor. We gain the core string (the real name) and the array of suffixes
	// such as "inc", "ltd" or "company"
	PreprocessVendorName(vendor, &core, &parts, &partsLength);
	uint32_t foundIndex = 0;
	// Since we do not know the number of matches, ensure that the allocated space will be enough
	uint32_t *foundIndices = (uint32_t *) malloc(sizeof(uint32_t) * registeredVendorsCount);
	ALLOCATED_POINTER_CHECK(foundIndices);
	float *foundSims = (float *) malloc(sizeof(float) * registeredVendorsCount);
	ALLOCATED_POINTER_CHECK(foundSims);
	// Effectively compare vendors in parallel using OpenMP
	#pragma omp parallel for
	for (uint32_t i = 0; i < registeredVendorsCount; i++) {
		float sim = VendorStringDistance(core, preprocessedVendors[i].Name,
			parts, preprocessedVendors[i].Suffixes, partsLength, preprocessedVendors[i].SuffixCount);
		if (sim > THRESHOLD) {
			// Only one vendor push at a time
			#pragma omp critical
			{
				foundSims[foundIndex] = sim;
				foundIndices[foundIndex++] = i;
			}
		}
	}
	// Output the matches
	VendorList list;
	list.count = foundIndex;
	list.vendors = 0;
	if (foundIndex > 0) {
		list.vendors = (VendorData *) malloc(sizeof(VendorData) * foundIndex);
		for (uint32_t i = 0; i < foundIndex; i++) {
			list.vendors[i].id = originalVendors[foundIndices[i]].Id;
			list.vendors[i].name = (char *) malloc(strlen(originalVendors[foundIndices[i]].Name) + 1);
			strcpy(list.vendors[i].name, originalVendors[foundIndices[i]].Name);
			list.vendors[i].similarity = foundSims[i];
		}
	}
	// Sort the matches from the least similar to the most similar
	// Old good qsort here, though the typical number of matches is small
	// so hard coded bubble sort is acceptable
	qsort(list.vendors, foundIndex, sizeof(VendorData), CompareFuncForQSortInFindSimilarVendors);
	
	// Clean up
	free(core);
	free(parts);
	free(foundIndices);
	free(foundSims);
	#pragma omp atomic
	Users--;
	return list;
}

// Frees the resources allocated by SimilarVendorsInitialize()
void SimilarVendorsFinalize()
{		
	int res;
	PTHREAD_INVOKE(pthread_mutex_lock, "pthread_mutex_lock", &lock);
	while (Users > 0) {
		usleep(10);
	}
	Users = -1;
	for (uint32_t i = 0; i < registeredVendorsCount; i++) {
		free(preprocessedVendors[i].Name);
		free((void *)preprocessedVendors[i].Suffixes);
		free(originalVendors[i].Name);
		free((void *)originalVendors[i].Suffixes);
	}
	if (preprocessedVendors) {
		free(preprocessedVendors);
	}
	if (originalVendors) {
		free(originalVendors);
	}
	FreeCommonSuffixes();
	PTHREAD_INVOKE(pthread_mutex_unlock, "pthread_mutex_unlock", &lock);
}

void FreeVendorData(VendorList list)
{
	if (!list.vendors) return;
	for (uint32_t i = 0; i < list.count; i++) {
		free(list.vendors[i].name);
	}
	free(list.vendors);
}

uint32_t GetVendorListLength(VendorList list)
{
	return list.count;
}

VendorData GetVendorFromList(VendorList list, uint32_t index)
{
	VendorData ret;
	if (index > list.count - 1) {
		ret.name = 0;
		ret.similarity = -1.0f;
		ret.id = 0;
		return ret;
	}
	return list.vendors[index];
}
